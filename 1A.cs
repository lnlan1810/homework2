using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = Convert.ToInt32(Console.ReadLine());
            for(int i =1; i <= n; i++)
            {
                for(int j =1; j <= 2*n-1; j++)
                {
                    if (j >= n - i + 1 && j <= n + i - 1)
                    {
                        Console.Write("*");
                    }
                    else Console.Write(" ");
                }
                Console.WriteLine();
            }
        }
    }
}
